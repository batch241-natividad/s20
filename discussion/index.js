// alert("hello")


// Repetition Control Structures

// While Loop

/*
	A while loop takes in an expression/condition 
	if the condition evaluates to be true, the statements inside the code will be executed

	Syntax:
		while(expression/condition){
			statement

		}

*/

let count = 5;

//While the value of count is not equal to zero, it will run

while (count !== 0){

	//The current value of count is printed out
	console.log("while " + count)


	//Decreases the value of count by 1 after every iteration
	count--;

}

//Do while loo

/*
- A do-while loop works a lot like the while loop but it guarantees that the code will be executed atleast once.

	-Syntax:
		do{
		statement


		} while(expression/condition)
*/

let number =Number(prompt("Give me a number: "))

do {
	// the current value of number is printed out
	console.log("Do while: " + number)
	
//Increases the value of number by 1 after every iteration to stop the loop 
//number = number + 1
	number += 1;


//Providing a number of 10 or greater will run the code block once but the loop will stop there
} while(number < 10);

//For Loop
/*
	-A for loop is more flexible than while and do-while loops.
	-It consists of three parts
		1. The initial value which is the starting value. The count will start at the initial value.
		2.The expression/condition that will determine if the loop will run one more time. While this is true, the loop will run.
		3.The final expression which determis how ther loop will run(Increment or decrement)

	Syntax:
		for(initial, expression/condition, finalExpression){
			statement
		}

*/

// for (let count = 0; count <=20; count++) {
// 	console.log(count)

// }


// //Characters in string may be counted using the .length property
// //Strings are special compared to other data types as it has access to functions and other pieces of information. Other primitive data doesn't have this.
// let myString ="jeru"
// console.log(myString.length)
// // Accessing elements of a string 
// //Individual characters of a string may be accesed using its index number
// //The first character in a string has an index of "0". The next is one and so on.
// // console.log(myString[0])
// // console.log(myString[1])
// // console.log(myString[2])
// // console.log(myString[3])


// // This loops prints the individual letters of the myString variable
// for (let x = 0; x < myString.length; x++){
// 	console.log(myString[x])
// }



// 	-This loop will print out the letters of the name individually but will print 3 if the letter is a vowel.
// 	-This loop starts with the value of zero assigned to i
// 	- This loop will run as long as the value of i is less than th length of the name.
// 	-This loop increments.


// let myName = "johndaniel";

// for (let i = 0; i < myName.length; i++){


// 	// If the character of your name is a vowel letter, instead of displaying the character, it will display the number of 3
// 	// the "toLowerCase" function will change the current letter being evaluated into small letters/lowercase
// 	if( 
// 		myName[i].toLowerCase() == "a" ||
// 		myName[i].toLowerCase() == "e" ||
// 		myName[i].toLowerCase() == "i" ||
// 		myName[i].toLowerCase() == "o" ||
// 		myName[i].toLowerCase() == "u" 

// 		) {

// 		//print 3 if the condition is met
// 		console.log();
// 	} else {

// 		// print the non vowel characters
// 		console.log(myName[i]);
// 	}
// }

//Continue and Break

/*
	-The "Continue" statement allows the code to go to the next iteration of a loop without finishing the execution of a statements in a code block
	-The "break" statement is used to terminate the loop once a match has been found.


*/

// for (let count = 0; count <= 20; count++) {

// //If remainder is equal to 0
// 	if(count % 2 === 0){
// 		continue;
// 		//tells the code to continue to the next iteration of the loop
// 		//Ignores all the statements located after the continue statement
// 	}
// 		console.log("Continue and Break: " + count)


// 		if (count > 10) {
// 		//Tells the code to terminate/stop the loop even if the condition/expression of the loop defines that it should execute so long as the value of count is less than or equal to 20
// 			break;
// 		}
// }

let name = "jakedlexter";

for (let i = 0; i < name.length; i++) {
	// the current letter is printed out based on its index
	console.log(name[i]);

	//if the value is a, continue to the next iteration of the loop
	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration");
		continue;
	}
	// if the current letter is equal to d, stop the loop
	if(name[i] == "d"){
		break;
	}

}