
var number = parseInt(prompt("Enter a number"), 10);
console.log("The number you provided is " + number);
for (var i = number; i >= 0; i--) {
    if (i <= 50) {
    	console.log("The number value is at 50. Terminating the loop.")
        break;
    }

    if (i % 10 === 0) {
        console.log("The number is divisible by 10. Skipping the number.");
        continue;
    }
    if (i % 5 === 0) {
        console.log(i);
    }
}

var str = "supercalifragilisticexpialidocious";
console.log(str);
var consonants = "";

for (var i = 0; i < str.length; i++) {
    if (str[i] === "a" || 
    	str[i] === "e" || 
    	str[i] === "i" || 
    	str[i] === "o" || 
    	str[i] === "u") {
        continue;
    } else {
        consonants += str[i];
    }
}
console.log(consonants);

